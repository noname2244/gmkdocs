#!/usr/bin/python
# -*- coding: utf-8 -*-

import sys, os, subprocess, ConfigParser, time, signal, shutil, re
from PyQt4 import QtGui
from PyQt4 import QtCore
from PyQt4.QtWebKit import *
from PyQt4.QtCore import QUrl
from time import gmtime, strftime
from subprocess import Popen, PIPE
reload(sys)
sys.setdefaultencoding('utf8')

def transliterate(name):
		# Слоаврь с заменами
	slovar = {'а':'a','б':'b','в':'v','г':'g','д':'d','е':'e','ё':'e',
      'ж':'zh','з':'z','и':'i','й':'i','к':'k','л':'l','м':'m','н':'n',
      'о':'o','п':'p','р':'r','с':'s','т':'t','у':'u','ф':'f','х':'h',
      'ц':'c','ч':'cz','ш':'sh','щ':'scz','ъ':'','ы':'y','ь':'','э':'e',
      'ю':'u','я':'ja', 'А':'a','Б':'b','В':'v','Г':'g','Д':'d','Е':'e','Ё':'e',
      'Ж':'zh','З':'z','И':'i','Й':'i','К':'k','Л':'l','М':'m','Н':'n',
      'О':'o','П':'p','Р':'r','С':'s','Т':'t','У':'u','Ф':'Х','х':'h',
      'Ц':'c','Ч':'cz','Ш':'sh','Щ':'scz','Ъ':'','Ы':'y','Ь':'','Э':'e',
      'Ю':'u','Я':'ja',',':'','?':'',' ':'_','~':'','!':'','@':'','#':'',
      '$':'','%':'','^':'','&':'','*':'','(':'',')':'','-':'','=':'','+':'',
      ':':'',';':'','<':'','>':'','\'':'','"':'','\\':'','/':'','№':'',
      '[':'',']':'','{':'','}':'','ґ':'','ї':'', 'є':'','Ґ':'g','Ї':'i',
      'Є':'e'}
        
		# Циклически заменяем все буквы в строке
	for key in slovar:
		name = name.replace(key, slovar[key])
	return name

class main(QtGui.QMainWindow):
    def __init__(self, parent=None):
	QtGui.QMainWindow.__init__(self)
	centralWidget = QtGui.QWidget()
	screen = QtGui.QDesktopWidget().screenGeometry()
	self.setGeometry(0, 0, 1024, 768)
	self.setWindowTitle('GMkDocs')
	self.setWindowIcon(QtGui.QIcon('icons/web.ico'))
	self.config = ConfigParser.RawConfigParser(allow_no_value=True)
	self.config.read('GMkDocs.cfg')
	self.filen = ''
	if not(self.config.has_option('main', 'mkdocs_dir')):
		self.selecthydedir()
	self.filen = self.config.get('main', 'mkdocs_dir')
	self.statusBar().showMessage(u'Текущая директория проекта: '+ self.filen)
	self.treeView = QtGui.QTreeView(self)
	self.fileSystemModel = QtGui.QFileSystemModel(self.treeView)
	self.fileSystemModel.setReadOnly(False)
	root = self.fileSystemModel.setRootPath(self.filen+'')
	self.indexRoot = self.fileSystemModel.index(self.fileSystemModel.rootPath())
	self.treeView.setModel(self.fileSystemModel)
	self.treeView.setRootIndex(root)
	self.treeView.clicked.connect(self.on_treeView_clicked)
	self.treeView.hideColumn(1)
	self.treeView.hideColumn(3)
	self.treeView.setColumnWidth(0, 300)
	self.inp = QtGui.QTextEdit(self)
	self.inp.hide()
	self.web = QWebView(self)
        size =  self.geometry()
        self.move((screen.width()-size.width())/2, (screen.height()-size.height())/2)
        self.choose = QtGui.QAction(QtGui.QIcon('images/open.png'), u'Выбрать расположение проекта', self)
        self.connect(self.choose, QtCore.SIGNAL('triggered()'), self.selecthydedir)
        self.add = QtGui.QAction(QtGui.QIcon('images/add.png'), u'Добавить страницу', self)
        self.connect(self.add, QtCore.SIGNAL('triggered()'), self.addnote)
	self.save =  QtGui.QAction(QtGui.QIcon('images/save.png'), u'Сохранить', self)
	self.save.setShortcut('Ctrl+S')
	self.connect(self.save, QtCore.SIGNAL('triggered()'), self.savenote)
	self.delete = QtGui.QAction(QtGui.QIcon('images/file_delete.png'), u'Удалить файл', self)
	self.connect(self.delete, QtCore.SIGNAL('triggered()'), self.delfile)
	self.toolbar = self.addToolBar('Main')
        self.toolbar.addAction(self.choose)
	self.toolbar.addAction(self.save)
	self.toolbar.addAction(self.add)
	self.toolbar.addAction(self.delete)
	self.pasteimg = QtGui.QAction(QtGui.QIcon('images/image-add-icon.png'), u'Вставить изображение', self)
	self.connect(self.pasteimg, QtCore.SIGNAL('triggered()'), self.pimg)
	self.pastelink = QtGui.QAction(QtGui.QIcon('images/Link.png'), u'Вставить ссылку на другую страницу', self)
	self.connect(self.pastelink, QtCore.SIGNAL('triggered()'), self.addlink)
	self.pastetable = QtGui.QAction(QtGui.QIcon('images/table.jpg'), u'Вставить таблицу', self)
	self.connect(self.pastetable, QtCore.SIGNAL('triggered()'), self.addtable)
	self.pasteseparator = QtGui.QAction(QtGui.QIcon('images/add_separator.png'), u'Вставить разделитель', self)
	self.connect(self.pasteseparator, QtCore.SIGNAL('triggered()'), self.addsep)
	self.pastevideo = QtGui.QAction(QtGui.QIcon('images/Add-video-icon-1106073050.png'), u'Вставить видео', self)
	self.connect(self.pastevideo, QtCore.SIGNAL('triggered()'), self.addvideo)
	self.pasteaudio = QtGui.QAction(QtGui.QIcon('images/book_audio_add-128.png'), u'Вставить аудио', self)
	self.connect(self.pasteaudio, QtCore.SIGNAL('triggered()'), self.addaudio)
	self.pasteextlink = QtGui.QAction(QtGui.QIcon('images/globe_links2.34673106_std.gif'), u'Вставить ссылку на другой Веб-сайт', self)
	self.connect(self.pasteextlink, QtCore.SIGNAL('triggered()'), self.addelink)
	self.pastebar = self.addToolBar('Edit')
	self.pastebar.addAction(self.pastelink)
	self.pastebar.addAction(self.pasteextlink)
	self.pastebar.addAction(self.pasteimg)
	self.pastebar.addAction(self.pastetable)
	self.pastebar.addAction(self.pasteseparator)
	self.pastebar.addAction(self.pastevideo)
	self.pastebar.addAction(self.pasteaudio)
        self.setFocus()
	self.newnotepath = ''
	self.treepath=''
	self.runserver()
	time.sleep(5)
	self.web.load(QUrl('http://localhost:8000'))
	self.web.show()
	def exitfunc():
		self.stopserver()
	sys.exitfunc = exitfunc
	gbox = QtGui.QGridLayout() 
	gbox.addWidget(self.treeView,0,0) 
	gbox.addWidget(self.inp,0,1)
	gbox.setColumnStretch(0,1)
	gbox.setColumnStretch(1,3)
	gbox.setColumnStretch(2,4)
	gbox.setColumnMinimumWidth(0, 100)
	gbox.addWidget(self.web,0,2)
	self.setCentralWidget(centralWidget)
	centralWidget.setLayout(gbox)
	self.statusBar().showMessage(u'Готово')
	
    def on_treeView_clicked(self, index):
	self.inp.setPlainText('')
	indexItem = self.fileSystemModel.index(index.row(), 0, index.parent())
        fileName = self.fileSystemModel.fileName(indexItem)
        filePath = self.fileSystemModel.filePath(indexItem)
	main.treepath=filePath
	if ((str(fileName[-3:]) == 'yml') or (str(fileName[-2:]) == 'md') or (str(fileName[-3:]) == 'css') or (str(fileName[-2:]) == 'js') or (str(fileName[-3:]) == 'xml')):
		self.inp.show()
		fil = open(filePath, 'rw')
		for line in fil.readlines():
			self.inp.insertPlainText(line.decode('utf8'))
		if (str(fileName[-2:]) == 'md') and (str(fileName[:-3]) != 'index'):
			self.web.load(QUrl('http://localhost:8000/'+str(fileName[:-3])+'/'))
		elif (str(fileName[-2:]) == 'md') and (str(fileName[:-3]) == 'index'):
			self.web.load(QUrl('http://localhost:8000'))
	else:
		self.inp.hide()
	
    def makesite(self):
	os.chdir(self.filen)
	os.system('mkdocs build --clean')
	self.statusBar().showMessage(u'Сайт создан в папке: '+ main.filen + '/site')
	
    def savenote(self):
	self.stopserver()
	if (self.newnotepath == ''):
		fil = open(str(self.treepath), 'w')
	else:
		fil = open(str(self.newnotepath), 'w')
		self.newnotepath = ''
	fil.write(str(main.inp.toPlainText()).encode('utf8'))
	self.makesite()
	self.runserver()
	self.web.reload()
        
    def addnote(self):
	text, ok = QtGui.QInputDialog.getText(self, u'Диалог', u'Введите имя страницы (Имя будет отображаться в колонке навигации сайта):')
	if (ok):
		self.inp.show()
		desc=text[:]
		text=transliterate(str(text))
		self.newnotepath = self.filen+'/docs/'+text+'.md'
		if not(os.path.exists(self.filen+'/docs/'+text)):	
			os.mkdir(self.filen+'/docs/'+text)
		self.inp.insertPlainText("")
		fil = open('mkdocs.yml', 'a')
		fil.write("- ['"+text+".md', '"+desc+"']\n")
		self.savenote()
		
    def selecthydedir(self):
        self.filen = str(QtGui.QFileDialog.getExistingDirectory(self, u'Выберите расположение вашего проекта'))
        self.statusBar().showMessage(u'Текущая директория проекта: '+ self.filen)
	if not(self.config.has_section('main')):
		self.config.add_section('main')
	self.config.set('main', 'mkdocs_dir', self.filen)
	with open('GMkDocs.cfg', 'wb') as configfile:
		self.config.write(configfile)
		
    def runserver(self):
        os.chdir(self.filen)
        self.t = subprocess.Popen(["mkdocs", "serve"], shell=False, stdout=None, stderr=None)
        self.statusBar().showMessage(u'Сайт доступен для просмотра по адресу: localhost:8000')
        
    def stopserver(self):
	os.kill(self.t.pid, signal.SIGTERM)
	time.sleep(5)
	self.t.terminate()
	
    def addlink(self):
	fileName = QtGui.QFileDialog.getOpenFileName(self, u'Выберите страницу:', '', selectedFilter='*.htm,*.html')
	if fileName:
		self.inp.insertPlainText('[link text]('+str(fileName[str(fileName).find('docs')+5:])+')')
		
    def addelink(self):
	self.inp.insertPlainText('[link text](http://yourlink.your)')
	
    def pimg(self):
	fileName = QtGui.QFileDialog.getOpenFileName(self, u'Выберите изображение:', '', selectedFilter='*.png,*.jpg,*.jpeg,*.gif')
	if fileName:
		if not(os.path.exists(self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')])):
			os.mkdir(self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')])
		shutil.copyfile(fileName, self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')]+'/'+os.path.basename(str(fileName)))
		self.inp.insertPlainText('![Alt-text]('+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')]+'/'+os.path.basename(str(fileName))+')')
	
    def delfile(self):
	wr=''
	fil = open('mkdocs.yml', 'r')	
	for st in fil.readlines():
		if st.find(str(os.path.basename(str(self.treepath)))) != -1:
			continue
		else:
			wr+=st
	fil = open('mkdocs.yml', 'w')
	fil.write(wr)
	for root, dirs, files in os.walk(str(self.treepath)[:-3], topdown=False):
		for name in files:
			os.remove(os.path.join(root, name))
		for name in dirs:
			os.rmdir(os.path.join(root, name))
	os.rmdir(self.treepath[:-3])	
	os.remove(self.treepath)
		
    def addsep(self):
	self.inp.insertPlainText('***')
	
    def addtable(self):
	text, ok = QtGui.QInputDialog.getText(self, u'Введите число строк и столбцов:', u'Формат: Строки,Столбцы')
	if ok:
		text = str(text).split(',')
		for y in range(0, int(text[1])+1):
			for x in range(0, int(text[0])):
				if (y == 1):
					self.inp.insertPlainText('|---')
				else:
					self.inp.insertPlainText('|   ')
			self.inp.insertPlainText('|\n')
			
    def addaudio(self):
	fileName = QtGui.QFileDialog.getOpenFileName(self, u'Выберите аудиофайл:', '', selectedFilter='*.mp3,*.wav,*.ogg')
	if fileName:
		if not(os.path.exists(self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')])):
			os.mkdir(self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')])
		shutil.copyfile(fileName, self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')]+'/'+os.path.basename(str(fileName)))
		self.inp.insertPlainText('<audio controls> \n <source src="'+os.path.basename(str(fileName))+'" type="audio/ogg"> \n </audio>')
	
    def addvideo(self):
	fileName = QtGui.QFileDialog.getOpenFileName(self, u'Выберите видеофайл', '', selectedFilter='*.mp4,*.webm,*.ogg')
	if fileName:
		if not(os.path.exists(self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')])):
			os.mkdir(self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')])
		shutil.copyfile(fileName, self.filen+'/docs/'+os.path.basename(str(self.treepath))[:os.path.basename(str(self.treepath)).find('.')]+'/'+os.path.basename(str(fileName)))
		self.inp.insertPlainText('<video width="320" height="240" controls>\n <source src="'+os.path.basename(str(fileName))+'" type="video/mp4"> \n </video>')
	  
	  
app = QtGui.QApplication(sys.argv)
main = main()
main.show()
sys.exit(app.exec_())
